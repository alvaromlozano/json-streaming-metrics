package es.amlozano.stream.twitter.test

import java.util.Properties

import com.typesafe.scalalogging.LazyLogging
import org.apache.kafka.clients.admin.{AdminClient, AdminClientConfig, NewTopic}
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.apache.kafka.common.serialization.Serde

import scala.collection.JavaConverters._

trait DockerizedKafka[K, V] extends LazyLogging {

  protected def topics: Seq[String]
  protected def serverHost: String = "localhost:9092"

  private lazy val kafkaAdminClient: AdminClient = AdminClient.create(
    Map[String, Object](
      AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG -> serverHost,
      AdminClientConfig.CLIENT_ID_CONFIG -> "test-kafka-admin-client"
    ).asJava
  )

  protected def createProducer(
    producerClientId: String,
    keySerde: Serde[K],
    valueSerde: Serde[V]
  ): KafkaProducer[K, V] = {
    val producerProperties: Properties = new Properties
    producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, serverHost)
    producerProperties.put(ProducerConfig.ACKS_CONFIG, "all")
    producerProperties.put(ProducerConfig.CLIENT_ID_CONFIG, producerClientId)
    producerProperties.put(ProducerConfig.BATCH_SIZE_CONFIG, 0)
    producerProperties.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 5000)
    new KafkaProducer[K, V](
      producerProperties,
      keySerde.serializer(),
      valueSerde.serializer()
    )
  }

  protected def createConsumer(
    consumerGroupId: String,
    consumerClientId: String,
    keySerde: Serde[K],
    valueSerde: Serde[V]
  ): KafkaConsumer[K, V] = {
    val consumerProperties = new Properties
    consumerProperties.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, true)
    consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serverHost)
    consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupId)
    consumerProperties.put(ConsumerConfig.CLIENT_ID_CONFIG, consumerClientId)
    consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    val consumer = new KafkaConsumer[K, V](
      consumerProperties,
      keySerde.deserializer(),
      valueSerde.deserializer()
    )
    consumer.subscribe(topics.asJava)
    consumer
  }

  protected def initialize(): Unit = {
    logger.info(s"Creating topics: $topics")
    kafkaAdminClient.createTopics(topics.map(name => new NewTopic(name, 1, 1.shortValue())).asJava)
  }

  protected def tearDown(): Unit = {
    logger.info(s"Deleting topic: $topics")
    kafkaAdminClient.deleteTopics(topics.asJava)
  }
}
