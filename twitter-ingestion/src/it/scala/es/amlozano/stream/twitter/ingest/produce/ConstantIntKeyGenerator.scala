package es.amlozano.stream.twitter.ingest.produce

class ConstantIntKeyGenerator[T] extends KeyGenerator[Int, T] {
  override def generateKey(message: T): Int = 1
}
