package es.amlozano.stream.twitter.ingest.produce

import java.time.Duration

import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import es.amlozano.stream.twitter.ingest.config.{Configuration, Settings}
import es.amlozano.stream.twitter.test.DockerizedKafka
import org.apache.kafka.common.serialization.Serdes.{IntegerSerde, StringSerde}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.Eventually
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration._
import scala.util.Random

class KafkaMessageWriterSpec
    extends AnyFunSpec
    with Matchers
    with Eventually
    with DockerizedKafka[Integer, String]
    with BeforeAndAfterAll
    with LazyLogging {
  private val settings: Configuration = new Settings(ConfigFactory.load)
  override protected def serverHost: String = settings.kafka.brokers
  private val random = new Random(this.getClass.getName.hashCode)
  private val testingTopics = Seq(s"${this.getClass.getName}-${random.nextInt()}")
  override implicit val patienceConfig: PatienceConfig = PatienceConfig(30.seconds, 200.milliseconds)
  override protected def topics: Seq[String] = testingTopics

  private val consumer = createConsumer(
    s"${this.getClass.getName}-consumer",
    s"${this.getClass.getName}-consumer",
    new IntegerSerde,
    new StringSerde
  )
  private val producer = createProducer(s"${this.getClass.getName}-producer", new IntegerSerde, new StringSerde)

  private def fixture = new {
    val keyGenerator: KeyGenerator[Int, String] = new ConstantIntKeyGenerator[String]
    val unit = new KafkaMessageWriter(producer, topics.head, keyGenerator)
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    initialize()
  }

  override def afterAll(): Unit = {
    tearDown()
    super.afterAll()
  }

  describe("A KafkaMessageWriter") {
    it("should write a single message") {
      val f = fixture

      f.unit.write("Hello")

      eventually {
        val batch = consumer.poll(Duration.ofMillis(100))
        batch.count() shouldBe 1
        val message = batch.iterator().next()
        message.key() shouldBe 1
        message.value() shouldBe "Hello"
      }
    }
  }
}
