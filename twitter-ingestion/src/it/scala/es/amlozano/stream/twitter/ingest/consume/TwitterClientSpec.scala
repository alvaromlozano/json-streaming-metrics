package es.amlozano.stream.twitter.ingest.consume

import com.twitter.hbc.core.endpoint.{StatusesSampleEndpoint, StreamingEndpoint}
import com.twitter.hbc.httpclient.auth.OAuth1
import com.typesafe.config.ConfigFactory
import es.amlozano.stream.twitter.ingest.config.{TwitterConfiguration, TwitterSettings}
import org.scalatest.{BeforeAndAfterEach, DoNotDiscover}
import org.scalatest.concurrent.Eventually
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

@DoNotDiscover
class TwitterClientSpec extends AnyFunSpec with Matchers with Eventually with BeforeAndAfterEach {
  import scala.concurrent.duration._
  override implicit val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = 10.seconds, interval = 10.milliseconds)

  private val fixture = new {
    val settings: TwitterConfiguration = new TwitterSettings(ConfigFactory.load())
    val endpoint: StreamingEndpoint = new StatusesSampleEndpoint
    val oAuth1: OAuth1 = new OAuth1(
      settings.twitterConsumerKey,
      settings.twitterConsumerSecret,
      settings.twitterToken,
      settings.twitterTokenSecret
    )
    val unit = new TwitterClient(oAuth1, endpoint)
  }

  override def beforeEach(): Unit = {
    super.beforeEach()
    fixture.unit.connect()
  }

  override def afterEach(): Unit = {
    super.beforeEach()
    fixture.unit.close()
  }

  describe("A TwitterClient") {
    it("should connect to twitter and read one message") {
      val f = fixture

      eventually {
        f.unit.nextTweet() match {
          case Some(_) => succeed
          case None => fail
        }
      }
    }
  }
}
