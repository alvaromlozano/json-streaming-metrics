package es.amlozano.stream.twitter.ingest

import java.util.Properties

import com.twitter.hbc.core.endpoint.StatusesSampleEndpoint
import com.twitter.hbc.httpclient.auth.OAuth1
import com.typesafe.config.ConfigFactory
import es.amlozano.stream.twitter.ingest.consume.TwitterClient
import es.amlozano.stream.twitter.ingest.config.{Configuration, Settings}
import es.amlozano.stream.twitter.ingest.produce.KafkaMessageWriter
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.apache.kafka.common.serialization.Serdes.{IntegerSerde, StringSerde}

import scala.sys.addShutdownHook

object TwitterStreamIngestion extends App {
  val settings: Configuration = new Settings(ConfigFactory.load)
  val sourceOAuth = new OAuth1(
    settings.twitter.twitterConsumerKey,
    settings.twitter.twitterConsumerSecret,
    settings.twitter.twitterToken,
    settings.twitter.twitterTokenSecret
  )
  val input = new TwitterClient(sourceOAuth, new StatusesSampleEndpoint)
  val topic = settings.kafka.topic
  val keyGenerator = new RandomIntKeyGenerator[String]
  val kafkaProperties: Properties = new Properties
  kafkaProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, settings.kafka.brokers)
  kafkaProperties.put(ProducerConfig.ACKS_CONFIG, "all")
  val kafkaProducer =
    new KafkaProducer[Integer, String](kafkaProperties, new IntegerSerde().serializer(), new StringSerde().serializer)
  val output = new KafkaMessageWriter(kafkaProducer, topic, keyGenerator)

  input.connect()

  addShutdownHook {
    input.close()
    output.close()
  }

  while (true) {
    output.write(input.nextTweet().get)
  }
}
