package es.amlozano.stream.twitter.ingest

import es.amlozano.stream.twitter.ingest.produce.KeyGenerator

import scala.util.Random

class RandomIntKeyGenerator[V] extends KeyGenerator[Int, V] {
  override def generateKey(message: V): Int = new Random().nextInt
}
