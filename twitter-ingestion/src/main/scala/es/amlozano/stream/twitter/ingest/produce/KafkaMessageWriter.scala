package es.amlozano.stream.twitter.ingest.produce

import com.typesafe.scalalogging.LazyLogging
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

class KafkaMessageWriter(
  producer: KafkaProducer[Integer, String],
  topic: String,
  keyGenerator: KeyGenerator[Int, String]
) extends MessageWriter[String]
    with LazyLogging {

  override def write(message: String): Unit = {
    logger.debug(s"Writing message to kafka: $message in topic $topic")
    producer.send(new ProducerRecord[Integer, String](topic, keyGenerator.generateKey(message), message))
  }

  override def close(): Unit = {
    logger.info("Closing Kafka Message Writer...")
    producer.close()
  }
}
