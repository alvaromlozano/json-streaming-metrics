package es.amlozano.stream.twitter.ingest.config

import com.typesafe.config.Config

class KafkaSettings(config: Config) extends KafkaConfiguration {
  override def brokers: String = config.getString("kafka.brokers")
  override def topic: String = config.getString("kafka.topic")
  override def offsetResetConfig: String = "earliest"
  override def readInterval: Long = 10000
}

class TwitterSettings(config: Config) extends TwitterConfiguration {
  override def twitterConsumerKey: String = config.getString("twitter.consumerKey")
  override def twitterConsumerSecret: String = config.getString("twitter.consumerSecret")
  override def twitterToken: String = config.getString("twitter.token")
  override def twitterTokenSecret: String = config.getString("twitter.tokenSecret")
}

class Settings(config: Config) extends Configuration {
  override def twitter: TwitterConfiguration = new TwitterSettings(config)
  override def kafka: KafkaConfiguration = new KafkaSettings(config)
}
