package es.amlozano.stream.twitter.ingest.produce

import java.io.Closeable

trait MessageWriter[V] extends Closeable {
  def write(message: V): Unit
}
