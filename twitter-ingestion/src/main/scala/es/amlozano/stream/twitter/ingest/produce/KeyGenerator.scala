package es.amlozano.stream.twitter.ingest.produce

trait KeyGenerator[K, V] {
  def generateKey(message: V): K
}
