package es.amlozano.stream.twitter.ingest.config

trait KafkaConfiguration {
  def brokers: String
  def topic: String
  def offsetResetConfig: String
  def readInterval: Long
}

trait TwitterConfiguration {
  def twitterConsumerKey: String
  def twitterConsumerSecret: String
  def twitterToken: String
  def twitterTokenSecret: String
}

trait Configuration {
  def twitter: TwitterConfiguration
  def kafka: KafkaConfiguration
}
