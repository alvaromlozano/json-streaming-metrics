name := "twitter-ingestion"
version := "0.1-SNAPSHOT"

libraryDependencies ++= Seq(
  Dependencies.Compile.kafka,
  Dependencies.Compile.twitterHbc,
  Dependencies.Compile.jackson,
  Dependencies.Compile.config,
  Dependencies.Compile.scalaLogging,
  Dependencies.Test.scalatest
)
