import sbt._

object Dependencies {
  val kafkaVersion = "2.5.0"
  val dockerTestKitVersion = "0.9.9"

  object Compile {
    val kafka = "org.apache.kafka" %% "kafka" % kafkaVersion
    val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaVersion
    val twitterHbc = "com.twitter" % "hbc-core" % "2.2.0"
    val jackson = "com.fasterxml.jackson.core" % "jackson-core" % "2.11.0"
    val config = "com.typesafe" % "config" % "1.4.0"
    val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
    val log4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.13.2"
  }

  object Provided {}

  object Test {
    val scalatest = "org.scalatest" %% "scalatest" % "3.1.1" % "it,test"
  }

}
