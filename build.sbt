import Dependencies.{Compile, Test}

name := "json-streaming-metrics"
organization in ThisBuild := "es.amlozano.json-streaming-metrics"

lazy val commonSettings =
  Seq(scalaVersion := "2.12.11")

lazy val commonDependencies =
  Seq(Compile.kafka, Compile.log4j, Test.scalatest)

lazy val global = project
  .in(file("."))
  .settings(commonSettings)
  .disablePlugins(AssemblyPlugin)
  .aggregate(ingestion)

lazy val ingestion = project
  .in(file("twitter-ingestion"))
  .configs(IntegrationTest)
  .settings(Defaults.itSettings, commonSettings, libraryDependencies ++= commonDependencies)
